// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/bloc/schulden_overzicht_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/organization_list.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/registrator_flow.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/client.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/registrator_organization.dart';
import 'package:blauwe_knop/repositories/schulden_request/request.dart';
import 'package:blauwe_knop/repositories/schulden_request/source_organization.dart';
import 'package:blauwe_knop/repositories/session_process/responses/start_challenge_response.dart';
import 'package:blauwe_knop/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import 'app_test.dart';

void main() {
  testWidgets(
    'test sorting of organizations listed',
    (WidgetTester tester) async {
      var schemeRepository = SchemeRepository(
        client: SchemeMemoryClient(
          organizations: [
            Organization(
              name: 'mock-org-a',
              apiUrl: '',
              loginUrl: '',
              oin: '001',
              isRegistrator: false,
              registratorUrl: '',
              appLinkProcessUrl: '',
              sessionProcessUrl: '',
            ),
            Organization(
              name: 'mock-org-b',
              apiUrl: '',
              loginUrl: '',
              oin: '002',
              isRegistrator: false,
              registratorUrl: '',
              appLinkProcessUrl: '',
              sessionProcessUrl: '',
            ),
            Organization(
              name: 'mock-org-c',
              apiUrl: '',
              loginUrl: '',
              oin: '003',
              isRegistrator: false,
              registratorUrl: '',
              appLinkProcessUrl: '',
              sessionProcessUrl: '',
            ),
          ],
        ),
      );

      var schuldenRequestRepository = MockSchuldenRequestRepository();

      when(schuldenRequestRepository.getRequest()).thenAnswer(
        (_) => Future<SchuldenRequest>.value(
          SchuldenRequest(
            id: '1',
            registratorOrganization: RegistratorOrganization(
              name: 'mock-registrator',
              oin: '002',
              debtRequestProcessUrl: '',
            ),
            sourceOrganizations: {
              '002': SourceOrganization(
                name: 'mock-org-b',
                oin: '002',
              ),
            },
          ),
        ),
      );

      var sessionProcessRepository = MockSessionProcessRepository();
      when(sessionProcessRepository.startChallenge('', null)).thenAnswer((_) =>
          Future<StartChallengeResponse>.value(
              StartChallengeResponse(date: 0, random: '')));

      await tester.pumpWidget(
        BlocProvider<SchuldenOverzichtBloc>(
          create: (_) {
            var result = SchuldenOverzichtBloc(
              schemeRepository,
              AuthorizationRepository(),
              MockAppLinkRepository(),
              MockDebtRepository(),
              AppDebtProcessRepository(
                client: MockSchuldenClient(),
              ),
              schuldenRequestRepository,
              sessionProcessRepository,
              MockKeyPairRepository(),
            );
            result.add(OrganizationsReload());
            return result;
          },
          child: MaterialApp(
            theme: Themes.blauweKnop(),
            home: Provider<KeyPairRepository>(
              create: (context) => MockKeyPairRepository(),
              child: Scaffold(
                body: RegistratorFlow(
                  onDismissHasSendDebtRequestConfirmation: () {},
                  onOrganizationTap: (_) {},
                  onRefreshHandle: (_) {},
                  onStartWizardTap: () {},
                ),
              ),
            ),
          ),
        ),
      );

      await tester.pumpAndSettle();

      var textOrganization = find
          .descendant(
            of: find.byType(OrganizationList),
            matching: find.byKey(
              Key('0'),
            ),
          )
          .evaluate()
          .first
          .widget as Text;

      expect(textOrganization.data, 'mock-org-b');
      textOrganization = find
          .descendant(
            of: find.byType(OrganizationList),
            matching: find.byKey(
              Key('1'),
            ),
          )
          .evaluate()
          .first
          .widget as Text;
      expect(textOrganization.data, 'mock-org-a');

      textOrganization = find
          .descendant(
            of: find.byType(OrganizationList),
            matching: find.byKey(
              Key('2'),
            ),
          )
          .evaluate()
          .first
          .widget as Text;
      expect(textOrganization.data, 'mock-org-c');
      await tester.pump(Duration(seconds: 3));
    },
  );
}
