// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

import 'package:blauwe_knop/models/faq.dart';
import 'package:blauwe_knop/pages/faq/faqs_page.dart';
import 'package:blauwe_knop/repositories/faq/client.dart';
import 'package:blauwe_knop/repositories/faq/repository.dart';
import 'package:blauwe_knop/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';

void main() {
  testWidgets('faq page', (WidgetTester tester) async {
    var faqRepository = FaqRepository(
      client: FaqMemoryClient(
        faqs: [
          Faq(
            question: 'Question #1',
            answer: 'Answer #1',
          )
        ],
      ),
    );

    await tester.pumpWidget(
      MaterialApp(
        theme: Themes.blauweKnop(),
        home: MultiProvider(
          providers: [
            Provider<FaqRepository>(
              create: (_) => faqRepository,
            ),
          ],
          child: FaqsPage(),
        ),
      ),
    );

    await tester.pumpAndSettle();

    expect(find.text('Question #1'), findsOneWidget);
    expect(find.text('Answer #1'), findsOneWidget);

    var box = tester.renderObject(find.byType(ExpansionPanelList)) as RenderBox;
    var oldHeight = box.size.height;

    await tester.tap(find.text('Question #1'));

    await tester.pumpAndSettle();

    box = tester.renderObject(find.byType(ExpansionPanelList)) as RenderBox;
    expect(box.size.height, greaterThan(oldHeight));
  });
}
