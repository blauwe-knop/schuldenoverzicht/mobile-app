// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht_detail/widgets/schulden_detail_list.dart';
import 'package:blauwe_knop/pages/schulden_overzicht_detail/widgets/schuld_list_item.dart';
import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:blauwe_knop/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('display schulden overzicht', (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        theme: Themes.blauweKnop(),
        home: SchuldenDetailList(
          SchuldenOverzicht(
            organisatie: 'mock-org',
            schulden: <SchuldDetail>[
              SchuldDetail(
                saldo: 249,
                saldoDatum: '2019-04-05T00:00:00Z',
                vorderingOvergedragen: false,
                type: 'mock-type-1',
              ),
              SchuldDetail(
                saldo: 150,
                saldoDatum: '2019-06-05T00:00:00Z',
                vorderingOvergedragen: true,
                type: 'mock-type-2',
              ),
            ],
            saldoDatumLaatsteSchuld: '2019-07-05T00:00:00Z',
            totaalSaldo: 399,
          ),
        ),
      ),
    );

    await tester.pumpAndSettle();

    expect(find.byType(SchuldListItem), findsNWidgets(2));

    expect(find.text('mock-org'), findsOneWidget);
    expect(find.text('2 schulden'), findsOneWidget);

    expect(find.text('mock-type-1'), findsOneWidget);
    expect(find.text('mock-type-2'), findsOneWidget);

    expect(find.text('€ 2,49'), findsOneWidget);
    expect(find.text('€ 1,50'), findsOneWidget);

    expect(find.text('op 05-04-2019'), findsOneWidget);
    expect(find.text('op 05-06-2019'), findsOneWidget);

    expect(find.text('Niet overgedragen'), findsOneWidget);
    expect(find.text('Vordering overgedragen'), findsOneWidget);

    expect(find.text('€ 3,99'), findsOneWidget);
    expect(find.text('Bij mock-org'), findsOneWidget);
    expect(find.byIcon(CupertinoIcons.question_circle), findsNWidgets(2));
  });
}
