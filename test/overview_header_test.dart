// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht/bloc/schulden_overzicht_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/overview_header.dart';
import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

void main() {
  testWidgets('found 3 debt org overview header', (WidgetTester tester) async {
    var debtInformations = <DebtInformation>[
      DebtInformation(
        schulden: SchuldenOverzicht(
          schulden: <SchuldDetail>[SchuldDetail()],
        ),
        status: ReceivedDebt(),
      ),
      DebtInformation(
        schulden: SchuldenOverzicht(
          schulden: <SchuldDetail>[SchuldDetail()],
        ),
        status: ReceivedDebt(),
      ),
      DebtInformation(
        schulden: SchuldenOverzicht(
          schulden: <SchuldDetail>[SchuldDetail()],
        ),
        status: ReceivedDebt(),
      )
    ];
    await tester.pumpWidget(MaterialApp(
      home: OverviewHeader(debtInformations),
    ));

    await tester.pumpAndSettle();

    expect(find.text('Schuldinformatie gecontroleerd bij 3 organisaties.'),
        findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is FaIcon && widget.icon == FontAwesomeIcons.clock),
        findsNothing);
  });

  testWidgets('waiting for 3 debt org overview header',
      (WidgetTester tester) async {
    var debtInformations = <DebtInformation>[
      DebtInformation(
        status: SourceOrganizationDebtRequested(),
      ),
      DebtInformation(
        status: SourceOrganizationDebtRequested(),
      ),
      DebtInformation(
        status: SourceOrganizationDebtRequested(),
      )
    ];
    await tester.pumpWidget(MaterialApp(
      home: OverviewHeader(debtInformations),
    ));

    await tester.pumpAndSettle();

    expect(find.text('Nog bezig met controleren bij 3 organisaties.'),
        findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is FaIcon && widget.icon == FontAwesomeIcons.clock),
        findsOneWidget);
  });

  testWidgets('found 1 debt org and waiting for 1 overview header',
      (WidgetTester tester) async {
    var debtInformations = <DebtInformation>[
      DebtInformation(
        schulden: SchuldenOverzicht(
          schulden: <SchuldDetail>[SchuldDetail()],
        ),
        status: ReceivedDebt(),
      ),
      DebtInformation(
        status: SourceOrganizationDebtRequested(),
      ),
    ];
    await tester.pumpWidget(MaterialApp(
      home: OverviewHeader(debtInformations),
    ));

    await tester.pumpAndSettle();

    expect(
        find.text(
            'Schuldinformatie gecontroleerd bij 1 organisatie, nog bezig met controleren bij 1 organisatie.'),
        findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is FaIcon && widget.icon == FontAwesomeIcons.clock),
        findsNothing);
  });

  testWidgets(
      'found 0 debt org, both unreachable and waiting for 0 overview header',
      (WidgetTester tester) async {
    var debtInformations = <DebtInformation>[
      DebtInformation(
        status: SourceOrganizationUnreachable(),
      ),
      DebtInformation(
        status: SourceOrganizationUnreachable(),
      ),
    ];
    await tester.pumpWidget(MaterialApp(
      home: OverviewHeader(debtInformations),
    ));

    await tester.pumpAndSettle();

    expect(find.text('Geen schuldinformatie beschikbaar.'), findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is FaIcon && widget.icon == FontAwesomeIcons.clock),
        findsNothing);
  });
}
