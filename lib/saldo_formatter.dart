// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:intl/intl.dart';

String formatSaldo(amountInCents) {
  var euroCurrency = NumberFormat.currency(locale: 'nl', symbol: '€');
  return euroCurrency.format(amountInCents / 100);
}
