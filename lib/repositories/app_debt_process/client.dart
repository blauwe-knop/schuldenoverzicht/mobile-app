// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

abstract class AppDebtProcessClient {
  Future<String> createAuthToken(String baseUrl);
  Future<String> exchangeAuthToken(
    String baseUrl,
    String authToken,
  );
  Future<String> getRequestIdForExchangeToken(
    String baseUrl,
    String exchangeToken,
  );
  Future<SchuldenOverzicht> getSchuldenOverzicht(
    String baseUrl,
    String token,
  );
}

class InvalidTokenException implements Exception {
  String errorMessage() {
    return 'Invalid token';
  }
}

class NoTokenProvidedException implements Exception {
  String errorMessage() {
    return 'No token provided';
  }
}

class SchuldenHTTPClient implements AppDebtProcessClient {
  @override
  Future<String> createAuthToken(String baseUrl) async {
    var url = Uri.parse('$baseUrl/authtoken/create');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw '[createAuthToken] unexpected status code: ${response.statusCode}';
    }
  }

  @override
  Future<String> exchangeAuthToken(String baseUrl, String authToken) async {
    var url = Uri.parse('$baseUrl/authtoken/exchange?authtoken=$authToken');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return response.body;
    } else if (response.statusCode == 400) {
      throw NoTokenProvidedException();
    } else if (response.statusCode == 401) {
      throw InvalidTokenException();
    } else {
      throw '[exchangeAuthToken] unexpected status code: ${response.statusCode}';
    }
  }

  @override
  Future<SchuldenOverzicht> getSchuldenOverzicht(
      String baseUrl, String token) async {
    var headers = <String, String>{};
    headers['Authorization'] = token;
    var url = Uri.parse('$baseUrl/schuldenoverzicht');
    var response = await http.get(
      url,
      headers: headers,
    );
    if (response.statusCode == 200) {
      return SchuldenOverzicht.fromJson(json.decode(response.body));
    } else if (response.statusCode == 403) {
      throw InvalidTokenException();
    } else if (response.statusCode == 400) {
      throw NoTokenProvidedException();
    } else {
      throw '[getSchuldenOverzicht] unexpected status code: ${response.statusCode}';
    }
  }

  @override
  Future<String> getRequestIdForExchangeToken(
      String baseUrl, String exchangeToken) async {
    var url = Uri.parse(
        '$baseUrl/debt-request-process/exchangetoken?requestExchangeToken=$exchangeToken');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      return null;
    }
  }
}
