import 'package:uni_links/uni_links.dart';

abstract class CallBackRepository {
  Stream<String> getUniversalLinkStream();
}

class UniLinkRepository implements CallBackRepository {
  Stream<String> callbackSteam;

  UniLinkRepository() {
    callbackSteam = linkStream;
  }
  @override
  Stream<String> getUniversalLinkStream() {
    return callbackSteam;
  }
}
