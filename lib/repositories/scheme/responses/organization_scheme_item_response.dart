// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

class OrganizationSchemeItem {
  String name;
  String apiBaseUrl;
  String loginUrl;
  String oin;
  String registratorUrl;
  bool isRegistrator;
  String appLinkProcessUrl;
  String sessionProcessUrl;

  OrganizationSchemeItem({
    this.name,
    this.apiBaseUrl,
    this.loginUrl,
    this.oin,
    this.registratorUrl,
    this.isRegistrator,
    this.appLinkProcessUrl,
    this.sessionProcessUrl,
  });

  OrganizationSchemeItem.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    apiBaseUrl = json['apiBaseUrl'];
    loginUrl = json['loginUrl'];
    oin = json['oin'];
    registratorUrl = json['registratorUrl'];
    isRegistrator = json['isRegistrator'];
    appLinkProcessUrl = json['appLinkProcessUrl'];
    sessionProcessUrl = json['sessionProcessUrl'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['apiBaseUrl'] = apiBaseUrl;
    data['loginUrl'] = loginUrl;
    data['oin'] = oin;
    data['registratorUrl'] = registratorUrl;
    data['isRegistrator'] = isRegistrator;
    data['appLinkProcessUrl'] = appLinkProcessUrl;
    data['sessionProcessUrl'] = sessionProcessUrl;
    return data;
  }
}
