// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

class RegistratorOrganization {
  String name;
  String oin;
  String debtRequestProcessUrl;

  RegistratorOrganization({
    this.name,
    this.oin,
    this.debtRequestProcessUrl,
  });

  RegistratorOrganization.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    oin = json['oin'];
    debtRequestProcessUrl = json['debtRequestProcessUrl'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['name'] = name;
    data['oin'] = oin;
    data['debtRequestProcessUrl'] = debtRequestProcessUrl;
    return data;
  }
}
