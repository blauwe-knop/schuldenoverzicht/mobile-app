import 'dart:convert';

import 'package:blauwe_knop/repositories/schulden_request/request.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

abstract class SchuldenRequestRepository {
  Future<SchuldenRequest> getRequest();
  Future<void> deleteRequest();
  Future<void> saveRequest(SchuldenRequest request);
}

class SchuldenRequestSecureStorage implements SchuldenRequestRepository {
  final _storage = FlutterSecureStorage();
  static const String _key = 'blauweknop.schulden.request';

  @override
  Future<void> deleteRequest() {
    return _storage.delete(key: _key);
  }

  @override
  Future<SchuldenRequest> getRequest() async {
    var requestString = await _storage.read(key: _key);
    if (requestString == null) {
      return null;
    }
    try {
      return SchuldenRequest.fromJson(json.decode(requestString));
    } catch (_) {
      await deleteRequest();
      return null;
    }
  }

  @override
  Future<void> saveRequest(SchuldenRequest request) {
    return _storage.write(key: _key, value: json.encode(request.toJson()));
  }
}
