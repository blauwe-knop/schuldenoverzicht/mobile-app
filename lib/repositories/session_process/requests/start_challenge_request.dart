class StartChallengeRequest {
  String publicKeyPEM;

  StartChallengeRequest({this.publicKeyPEM});

  StartChallengeRequest.fromJson(Map<String, dynamic> json) {
    publicKeyPEM = json['publicKeyPEM'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['publicKeyPEM'] = publicKeyPEM;
    return data;
  }
}
