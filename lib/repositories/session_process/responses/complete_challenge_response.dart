// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

class CompleteChallengeResponse {
  String sessionToken;
  CompleteChallengeResponse({this.sessionToken});
  CompleteChallengeResponse.fromJson(Map<String, dynamic> json) {
    sessionToken = json['sessionToken'];
  }
  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['sessionToken'] = sessionToken;
    return data;
  }
}
