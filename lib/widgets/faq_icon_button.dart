// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/faq/faqs_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class FaqIconButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        FontAwesomeIcons.questionCircle,
        size: 24,
      ),
      onPressed: () {
        Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => FaqsPage()),
        );
      },
    );
  }
}
