// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/authentication-wizard/bloc/wizard_bloc.dart';
import 'package:blauwe_knop/pages/authentication-wizard/widgets/back_button.dart';
import 'package:blauwe_knop/widgets/faq_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AllOrManualSelection extends StatelessWidget {
  static const String routeName = 'all_or_manual_selection';
  final void Function(bool) submitUseAllOrganizationsHandler;
  final void Function() onBackTapped;

  AllOrManualSelection({
    @required this.submitUseAllOrganizationsHandler,
    @required this.onBackTapped,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Stap 1 / 4',
        ),
        leading: CustomBackButton(
          onPressed: () => onBackTapped(),
        ),
        actions: [
          FaqIconButton(),
        ],
      ),
      body: BlocBuilder<WizardBloc, WizardState>(
        builder: (context, state) {
          return Center(
            child: state.organizations != null
                ? _buildOrganizationSelectionAllOrManual(
                    context,
                    state.organizations.length,
                    submitUseAllOrganizationsHandler)
                : Container(
                    width: 48,
                    height: 48,
                    child: CircularProgressIndicator(),
                  ),
          );
        },
      ),
    );
  }
}

Widget _buildOrganizationSelectionAllOrManual(BuildContext context,
    int organizationCount, Function(bool) submitUseAllOrganizationsHandler) {
  var menuOptions = [
    'Ja, controleer bij $organizationCount organisaties op schulden',
    'Nee, ik wil zelf de organisaties kiezen'
  ];

  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 35, vertical: 35),
        child: Text(
          'Met de Blauwe Knop kunt u gemakkelijk bij alle aangesloten organisaties informatie over uw schulden op halen. \n\nWilt u dit bij alle deelnemende overheidsorganisaties doen?',
          style: Theme.of(context).textTheme.headline1,
          textAlign: TextAlign.center,
        ),
      ),
      Expanded(
        child: ListView.separated(
          separatorBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.only(
                left: 16.0,
                right: 16.0,
              ),
              child: Divider(height: 1, color: Colors.grey[300]),
            );
          },
          itemBuilder: (context, index) {
            var menuOption = menuOptions[index];
            return InkWell(
              onTap: () {
                submitUseAllOrganizationsHandler(index == 0);
              },
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        menuOption,
                      ),
                    ),
                    Icon(
                      Icons.chevron_right,
                      color: Colors.grey,
                    )
                  ],
                ),
              ),
            );
          },
          itemCount: menuOptions.length,
        ),
      ),
    ],
  );
}
