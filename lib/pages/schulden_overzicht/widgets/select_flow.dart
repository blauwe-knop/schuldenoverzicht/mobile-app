// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

const SettingsPageLinkKey = Key('settings-page-link');

class SelectFlow extends StatelessWidget {
  final VoidCallback _onStartWizardCallPressed;
  final VoidCallback _onLoginPerOrganizationPressed;

  SelectFlow(
      {@required VoidCallback onStartWizardPressed,
      @required VoidCallback onLoginPerOrganizationPressed})
      : _onStartWizardCallPressed = onStartWizardPressed,
        _onLoginPerOrganizationPressed = onLoginPerOrganizationPressed;

  // Unused, but left here on purpose
  Widget LoginPerOrganization(BuildContext context) {
    return InkWell(
      onTap: () => _onLoginPerOrganizationPressed(),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            'Of',
          ),
          Text(
            ' per organisatie inloggen',
            style: Theme.of(context).textTheme.bodyText2.copyWith(
                  color: Theme.of(context).primaryColor,
                ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 370,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 48.0),
          SvgPicture.asset(
            'assets/bk.svg',
            height: 84,
            width: 64,
          ),
          SizedBox(height: 16.0),
          Flexible(
            child: Text(
              'Eén overzicht van uw schulden bij overheidsorganisaties',
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
          SizedBox(height: 16.0),
          Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
            ),
            child: Text(
              'Haal veilig uw schuldgegevens op. U hoeft maar één keer in te loggen',
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 16.0),
          Padding(
            padding: const EdgeInsets.only(
              left: 16.0,
              right: 16.0,
            ),
            child: _buildStartWizardButton(context),
          ),
          // LoginPerOrganization(context),
        ],
      ),
    );
  }

  Widget _buildStartWizardButton(BuildContext context) {
    return ElevatedButton(
      onPressed: () => _onStartWizardCallPressed(),
      style: ElevatedButton.styleFrom(
        primary: Theme.of(context).primaryColor,
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Stap 1: Kies organisaties',
              style: Theme.of(context)
                  .textTheme
                  .button
                  .copyWith(color: Colors.white),
            ),
            Icon(
              Icons.chevron_right,
              color: Colors.white,
            )
          ],
        ),
      ),
    );
  }
}
