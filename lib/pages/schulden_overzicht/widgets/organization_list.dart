// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht/bloc/schulden_overzicht_bloc.dart';
import 'package:blauwe_knop/saldo_formatter.dart';
import 'package:blauwe_knop/widgets/schuld_total.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:intl/intl.dart';

class OrganizationList extends StatelessWidget {
  final List<DebtInformation> organizations;
  final DateTime loadingTriggeredAt;
  final Function onRefresh;
  final bool isLoading;
  final Function(DebtInformation org) onOrganizationTap;
  final bool manualLogin;

  OrganizationList({
    @required this.organizations,
    @required this.isLoading,
    @required this.onOrganizationTap,
    @required this.manualLogin,
    this.loadingTriggeredAt,
    this.onRefresh,
  });

  @override
  Widget build(BuildContext context) {
    var dateFormat = DateFormat('HH:mm');
    var totaalSaldo = 0;

    organizations.forEach((o) {
      if (o.schulden != null) {
        totaalSaldo += o.schulden.totaalSaldo;
      }
    });

    _sortOrganizations(organizations);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 25),
          child: GestureDetector(
            child: Text(
              manualLogin
                  ? 'Deelnemende organisaties:'
                  : 'Uw schuldinformatie:',
              style: Theme.of(context).textTheme.headline1,
            ),
          ),
        ),
        Expanded(
          child: RefreshIndicator(
            onRefresh: () => onRefresh(),
            child: ListView.separated(
              separatorBuilder: (context, index) {
                return Divider(
                  thickness: 1,
                );
              },
              itemBuilder: (context, index) {
                if (index == organizations.length) {
                  return Text(
                    'Geladen op ${loadingTriggeredAt != null ? dateFormat.format(loadingTriggeredAt) : ''}',
                    textAlign: TextAlign.center,
                  );
                }
                var organization = organizations[index];

                return _buildListItemOrganization(index, context, organization);
              },
              itemCount: organizations.length + 1,
            ),
          ),
        ),
        TotalSchuld(
          totalSaldo: totaalSaldo,
        )
      ],
    );
  }

  void _sortOrganizations(List<DebtInformation> organizations) {
    organizations.sort((a, b) {
      if (a.schulden != null && b.schulden != null) {
        if (a.schulden.totaalSaldo == null && b.schulden.totaalSaldo != null) {
          return 1;
        }
        if (a.schulden.totaalSaldo != null && b.schulden.totaalSaldo == null) {
          return -1;
        }

        if (a.schulden.totaalSaldo == 0 && b.schulden.totaalSaldo != 0) {
          return 1;
        }

        if (a.schulden.totaalSaldo != 0 && b.schulden.totaalSaldo == 0) {
          return -1;
        }
      }

      if (a.status is SourceOrganizationUnreachable &&
          b.status is! SourceOrganizationUnreachable) {
        return 1;
      }

      if (a.status is! SourceOrganizationUnreachable &&
          b.status is SourceOrganizationUnreachable) {
        return -1;
      }

      if (a.status is NotIncludedInRequest &&
          b.status is! NotIncludedInRequest) {
        return 1;
      }

      if (a.status is! NotIncludedInRequest &&
          b.status is NotIncludedInRequest) {
        return -1;
      }

      if (a.status is Expired && b.status is! Expired) {
        return 1;
      }

      if (a.status is! Expired && b.status is Expired) {
        return -1;
      }

      return a.organization.name.compareTo(b.organization.name);
    });
  }

  Widget _buildListItemOrganization(
    int index,
    BuildContext context,
    DebtInformation organization,
  ) {
    var trailingIcon = organization.schulden != null
        ? Icon(Icons.chevron_right)
        : manualLogin && !organization.loggedIn
            ? Icon(
                Icons.lock,
                color: Theme.of(context).errorColor,
              )
            : null;
    return ListTile(
      onTap: () => onOrganizationTap(organization),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            organization.organization.name,
            key: Key(index.toString()),
            style: Theme.of(context).textTheme.bodyText2,
          ),
          if (organization.schulden != null)
            Text(
              formatSaldo(organization.schulden.totaalSaldo),
              style: Theme.of(context).textTheme.bodyText2,
            ),
        ],
      ),
      subtitle: !manualLogin && !(organization.status is ReceivedDebt)
          ? Row(
              children: <Widget>[
                FaIcon(
                  _getStatusIcon(context, organization.status),
                  size: Theme.of(context).iconTheme.size,
                  color: _getStatusColor(context, organization.status),
                ),
                SizedBox(
                  width: 4,
                ),
                Text(
                  _statusToString(organization.status),
                  style: TextStyle(
                      color: _getStatusColor(context, organization.status),
                      fontSize: 14),
                ),
              ],
            )
          : null,
      trailing: trailingIcon,
    );
  }
}

IconData _getStatusIcon(context, DebtRequestStatus status) {
  if (status is Expired || status is SourceOrganizationUnreachable) {
    return FontAwesomeIcons.exclamation;
  }
  return FontAwesomeIcons.clock;
}

Color _getStatusColor(context, DebtRequestStatus status) {
  if (status is Expired || status is SourceOrganizationUnreachable) {
    return Theme.of(context).errorColor;
  }
  return Theme.of(context).textTheme.bodyText1.color;
}

String _statusToString(DebtRequestStatus status) {
  if (status is IncludedInDebtRequest) {
    return 'Verzoek wordt ingediend';
  } else if (status is SourceOrganizationLinkRequested) {
    return 'Verzoek is ingediend';
  } else if (status is SourceOrganizationDebtRequested) {
    return 'Verzoek wordt behandeld';
  } else if (status is Expired) {
    return 'Niet meer gekoppeld';
  } else if (status is SourceOrganizationUnreachable) {
    return 'Organisatie onbereikbaar';
  } else if (status is NotIncludedInRequest) {
    return 'Niet opgenomen in het verzoek';
  }
  return 'Onverwachte fout opgetreden';
}
