// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/models/faq.dart';
import 'package:blauwe_knop/repositories/faq/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FaqsBloc extends Bloc<FaqsEvent, FaqsState> {
  final FaqRepository _faqRepository;

  FaqsBloc(
    this._faqRepository,
  ) : super(FaqsState()) {
    on<LoadFaqItems>(_onLoadFaqItems);
    on<FaqItemExpand>(_onFaqItemExpand);

    add(LoadFaqItems());
  }

  Future<void> _onLoadFaqItems(
      LoadFaqItems event, Emitter<FaqsState> emit) async {
    var faqItems = await _faqRepository.client.getFaqs();

    emit(state.copyWith(
      faqItems: faqItems.map((i) => FaqItem.fromFaq(i)).toList(),
    ));
  }

  void _onFaqItemExpand(FaqItemExpand event, Emitter<FaqsState> emit) {
    var faqItems = state.faqItems;

    faqItems[event.index].isExpanded = !event.isExpanded;

    emit(state.copyWith(
      faqItems: faqItems,
    ));
  }
}

class FaqItem extends Faq {
  bool isExpanded;

  FaqItem({
    this.isExpanded = false,
  });

  FaqItem.fromFaq(Faq faq) {
    question = faq.question;
    answer = faq.answer;
    isExpanded = false;
  }
}

class FaqsState {
  List<FaqItem> faqItems = [];

  FaqsState({
    this.faqItems,
  });

  FaqsState copyWith({
    List<FaqItem> faqItems,
  }) {
    return FaqsState(
      faqItems: faqItems ?? this.faqItems,
    );
  }
}

class FaqsEvent {
  const FaqsEvent();
}

class LoadFaqItems extends FaqsEvent {}

class FaqItemExpand extends FaqsEvent {
  int index;
  bool isExpanded;

  FaqItemExpand(this.index, this.isExpanded);
}
