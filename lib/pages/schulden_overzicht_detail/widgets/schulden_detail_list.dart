// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht_detail/widgets/schuld_header.dart';
import 'package:blauwe_knop/pages/schulden_overzicht_detail/widgets/schuld_list_item.dart';
import 'package:blauwe_knop/widgets/schuld_total.dart';
import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SchuldenDetailList extends StatelessWidget {
  final SchuldenOverzicht _schuldenOverzicht;

  SchuldenDetailList(this._schuldenOverzicht);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SchuldHeader(
          _schuldenOverzicht.organisatie,
          _schuldenOverzicht.schulden.length,
        ),
        Expanded(
          child: ListView.separated(
            separatorBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(
                  left: 16.0,
                  right: 16.0,
                ),
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 1,
                    color: Colors.grey[300]),
              );
            },
            itemBuilder: (context, index) {
              var schuld = _schuldenOverzicht.schulden[index];
              return SchuldListItem(schuld);
            },
            itemCount: _schuldenOverzicht.schulden.length,
          ),
        ),
        TotalSchuld(
          organization: _schuldenOverzicht.organisatie,
          totalSaldo: _schuldenOverzicht.totaalSaldo,
        )
      ],
    );
  }
}
