// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class ClaimHintIcon extends StatefulWidget {
  @override
  _ClaimHintIconState createState() => _ClaimHintIconState();
}

class _ClaimHintIconState extends State<ClaimHintIcon> {
  bool showOverlay = false;
  OverlayEntry _overlayEntry;
  final LayerLink _layerLink = LayerLink();
  final GlobalKey _keyIcon = GlobalKey();

  void handlePressed() {
    setState(() {
      showOverlay = !showOverlay;
    });

    if (showOverlay) {
      _overlayEntry = _createOverlayEntry();
      Overlay.of(context).insert(_overlayEntry);
    } else {
      _overlayEntry.remove();
    }
  }

  OverlayEntry _createOverlayEntry() {
    final RenderBox renderBoxIcon = _keyIcon.currentContext.findRenderObject();
    final iconPosition = renderBoxIcon.localToGlobal(Offset.zero);
    final padding = 15;
    return OverlayEntry(
      builder: (context) => Positioned(
        height: 135,
        width: MediaQuery.of(context).size.width - padding * 2,
        child: CompositedTransformFollower(
            link: _layerLink,
            showWhenUnlinked: false,
            followerAnchor: Alignment.bottomCenter,
            offset: Offset(
                MediaQuery.of(context).size.width / 2 -
                    iconPosition.dx +
                    renderBoxIcon.size.width / 4,
                0),
            child: Material(
              shadowColor: Colors.black,
              shape: TooltipShapeBorder(
                  arrowArc: 0.5, arrowXPosition: iconPosition.dx + 2),
              elevation: 8.0,
              child: Padding(
                padding:
                    EdgeInsets.only(left: 14, top: 13, right: 9, bottom: 13),
                child: Text(
                  'Of de inning van de vordering is overgedragen aan een incassobureau. Deze informatie is relevant bij schuldhulpverlening.',
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return CompositedTransformTarget(
      link: _layerLink,
      child: CupertinoButton(
        minSize: 20,
        padding: EdgeInsets.all(3),
        onPressed: handlePressed,
        child: Icon(
          CupertinoIcons.question_circle,
          color: Theme.of(context).iconTheme.color,
          size: Theme.of(context).iconTheme.size,
          key: _keyIcon,
        ),
      ),
    );
  }
}

class TooltipShapeBorder extends ShapeBorder {
  final double arrowWidth;
  final double arrowHeight;
  final double arrowXPosition;
  final double arrowArc;
  final double radius;

  TooltipShapeBorder({
    this.radius = 4,
    this.arrowWidth = 20,
    this.arrowHeight = 10,
    this.arrowXPosition = 0,
    this.arrowArc = 0,
  }) : assert(arrowArc <= 1 && arrowArc >= 0);

  @override
  EdgeInsetsGeometry get dimensions => EdgeInsets.only(bottom: arrowHeight);

  @override
  Path getInnerPath(Rect rect, {TextDirection textDirection}) => null;

  @override
  Path getOuterPath(Rect rect, {TextDirection textDirection}) {
    rect = Rect.fromPoints(
        rect.topLeft, rect.bottomRight - Offset(0, arrowHeight));
    var x = arrowWidth;
    var y = arrowHeight;
    var r = 1 - arrowArc;
    return Path()
      ..addRRect(RRect.fromRectAndRadius(rect, Radius.circular(radius)))
      ..moveTo(arrowXPosition, rect.bottomCenter.dy)
      ..relativeLineTo(-x / 2 * r, y * r)
      ..relativeQuadraticBezierTo(
          -x / 2 * (1 - r), y * (1 - r), -x * (1 - r), 0)
      ..relativeLineTo(-x / 2 * r, -y * r);
  }

  @override
  void paint(Canvas canvas, Rect rect, {TextDirection textDirection}) {}

  @override
  ShapeBorder scale(double t) => this;
}
