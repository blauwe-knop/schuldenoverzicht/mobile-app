// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/widgets/header.dart';
import 'package:flutter/material.dart';

class SchuldHeader extends StatelessWidget {
  final String organization;
  final int schuldCount;

  SchuldHeader(this.organization, this.schuldCount);

  @override
  Widget build(BuildContext context) {
    return Header(
      <Widget>[
        Text(
          organization,
          style: Theme.of(context).textTheme.headline1,
        ),
        SizedBox(height: 4.0),
        Text('$schuldCount schulden',
            style: Theme.of(context).textTheme.bodyText1),
      ],
    );
  }
}
