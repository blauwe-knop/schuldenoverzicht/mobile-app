# blauwe_knop

This is the Blauwe Knop app!

## Getting Started

The only thing you need to get this project up and running is [Flutter](https://flutter.dev/docs/get-started/install)

## Data retention

The app can store a request to obtain schulden on your device(the Keychain is used for iOS), this data will not be removed when the app is uninstalled.

## Running App

### Production

```sh
flutter run
```

#### IOS

Run and debug mobile-app `local ios` from Visual Code Studio. (see launch.json)

Or via the terminal

```sh
flutter run -t lib/main_local.dart
```

#### Android

Run and debug mobile-app `local android` from Visual Code Studio. (see launch.json)

Or via the terminal

```sh
flutter run -t lib/main_local_android.dart
```

\*_And also the backend services:_./deplo\*

```sh
cd ../blauwe-knop && modd
```

> Note: When you use Virtual Android Device, you need to run:

```sh
cd ../blauwe-knop && modd -f modd-android.conf
```

## Test

```sh
flutter test
```

### Coverage

Install lcov (Mac):

```sh
brew install lcov
```

Run tests, generate coverage files and convert to HTML:

```sh
flutter test --coverage
genhtml coverage/lcov.info -o coverage/html
```

Open `coverage/html/index.html` in your browser.
